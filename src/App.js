import "./App.css";
import { useEffect, useState } from "react";
import Axios from "axios";
import { Link, Route, Switch } from "react-router-dom";
import Home from "./components/home";
import Item from "./components/item";
import Store from "./components/store";
import Login from "./components/login";
import Register from "./components/register.js"
import Profile from "./components/profile";
import EditProfile from "./components/edit_profile";
import Register_Product from "./components/register_product";
import Product from "./components/product";
import EditProduct from "./components/edit_product";
import Cart from "./components/cart";
import Redeem from "./components/redeem";

function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/store">
          <Store />
        </Route>
        <Route exact path="/profile/:user_url">
          <Profile />
        </Route>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/register">
        <Register />
        </Route>
        <Route path="/edit_profile/:user_url">
        <EditProfile />
        </Route>
        <Route path="/edit_product/:product_url">
        <EditProduct />
        </Route>
        <Route path="/register_product">
          <Register_Product />
        </Route>
        <Route path="/product/:product_url">
          <Product />
        </Route>
        <Route path="/cart">
          <Cart />
        </Route>
        <Route path="/redeem/:user_url">
          <Redeem />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
