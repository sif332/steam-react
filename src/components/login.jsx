import { useState } from "react";
import Axios from "axios";
import "./login.css";
import Navbar,{globalLogState} from "./navbar";
import { Link } from "react-router-dom";

function Login() {
  Axios.defaults.withCredentials = true;
  const [logState, setLogIn] = useState(false);
  const [passCheck, setpass] = useState(true);
  const [usernameState, usernameSet] = useState("");
  const [passwordState, passwordSet] = useState("");

  const loginUser = (event) => {
    Axios.post("https://steam-server-api.herokuapp.com/login", {
      username: usernameState,
      password: passwordState,
    }).then((res) => {
      if (res.data.isPass) {
        console.log("is Login pass?", res.data.isPass);
        Navbar.globalLogState=true;
        setLogIn(true);
        setpass(true);
      } else {
        console.log("is Login pass?", res.data.isPass);
        Navbar.globalLogState=false;
        setLogIn(false);
        setpass(false);
      }
    });
    event.preventDefault();
  };

  if (logState) {
    return (
      <div>
        <Navbar />
        <div className="login form-signin">
          <form>
            <Link className="w-100 btn btn-lg btn-primary" to="/">
              Home
            </Link>
          </form>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <Navbar />

        <div className="login form-signin">
          <form>
            <div className="mb-3">
              <h1 class="h3 mb-3 fw-normal text-light" id="please_sign_in">
                Please sign in
              </h1>
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="name@example.com"
                  onChange={(event) => {
                    usernameSet(event.target.value);
                  }}
                />
                <label for="floatingInput">Username</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  type="password"
                  class="form-control"
                  id="floatingPassword"
                  placeholder="Password"
                  onChange={(event) => {
                    passwordSet(event.target.value);
                  }}
                />
                <label for="floatingPassword">Password</label>
              </div>
            </div>
            {passCheck === false && <p id="pass_incorrect" >username or password is incorrect</p>}
            <button
              className="w-100 btn btn-lg btn-primary"
              onClick={loginUser}
            >
              Login
            </button>
            <hr></hr>
            <Link className="w-100 btn btn-lg btn-primary" to="/register">
              Register
            </Link>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
