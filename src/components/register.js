import { useState } from "react";
import Axios from "axios";
import "./register.css";
import Navbar from "./navbar";

import { Link } from "react-router-dom";

function Register() {
  Axios.defaults.withCredentials = true;
  const [alreadyRegister, setAlreadyRegister] = useState(false);
  const [registerCheck, setRegisterCheck] = useState(true);

  const [usernameState, usernameSet] = useState("");
  const [passwordState, passwordSet] = useState("");
  const [passwordState2, passwordSet2] = useState("");

  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [payment, setPayment] = useState("");

  const addUser = (event) => {
    Axios.post("https://steam-server-api.herokuapp.com/register", {
      username: usernameState,
      password: passwordState,
      firstname: firstname,
      lastname: lastname,
      email: email,
      payment: parseInt(payment)
    }).then((res) => {
      if(res.data.isRegister){
        console.log(res.data.message);
        setRegisterCheck(true);
        setAlreadyRegister(true);
        console.log("Register Successfully");
      }else{
        console.log(res.data.message);
        console.log("Duplicate Username");
        setRegisterCheck(false);
        setAlreadyRegister(false);
      }
    });
    event.preventDefault();
  };

  if (alreadyRegister) {
    return (
      <div>
        <Navbar />
        <div className="login form-signin">
          <form>
            <Link className="w-100 btn btn-lg btn-primary" to="/login">
              Login
            </Link>
          </form>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <Navbar />

        <div className="login form-signin">
          <form>
            <div className="mb-3">
              <h1 class="h3 mb-3 fw-normal text-light" id="please_sign_in">
                Registration
              </h1>
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="username"
                  onChange={(event) => {
                    usernameSet(event.target.value);
                  }}
                />
                <label for="floatingInput">Username</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  type="password"
                  class="form-control"
                  id="floatingPassword"
                  placeholder="Password"
                  onChange={(event) => {
                    passwordSet(event.target.value);
                  }}
                />
                <label for="floatingPassword">Password</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  type="password"
                  class="form-control"
                  id="floatingPassword"
                  placeholder="Password"
                  onChange={(event) => {
                    passwordSet2(event.target.value);
                  }}
                />
                <label for="floatingPassword">Confirm Password</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="firstname"
                  onChange={(event) => {
                    setFirstname(event.target.value);
                  }}
                />
                <label for="floatingInput">Firstname</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="lastname"
                  onChange={(event) => {
                    setLastname(event.target.value);
                  }}
                />
                <label for="floatingInput">Lastname</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="email"
                  onChange={(event) => {
                    setEmail(event.target.value);
                  }}
                />
                <label for="floatingInput">Email</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="firstname"
                  onChange={(event) => {
                    setPayment(event.target.value);
                  }}
                />
                <label for="floatingInput">Payment Number</label>
              </div>
            </div>
            {registerCheck === false && <p id="pass_incorrect" >Username or Email has been already existed.</p>}
            {passwordState !== passwordState2 ? <p id="pass_incorrect" >Passwords does not match</p> :
            <button
              className="w-100 btn btn-lg btn-primary"
              onClick={addUser}
            >
              Register
            </button>
            }
          </form>
        </div>
      </div>
    );
  }
}

export default Register;
