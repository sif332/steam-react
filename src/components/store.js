import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Axios from "axios";
import Navbar from "./navbar";

Axios.defaults.withCredentials = true;

function Store() {
  const [productList, setProductList] = useState([]);

  function buyProduct(myproduct_id) {
    Axios.get("https://steam-server-api.herokuapp.com/buy/" + myproduct_id).then((res) => {
      if (res.data) {
      } else {
      }
    });
  }

  useEffect(() => {
    Axios.get("https://steam-server-api.herokuapp.com/store").then((res) => {
      if (res.data) {
        setProductList(res.data.product_list);
        console.log("Store Found");
      } else {
        console.log("Store not found");
      }
    });
  }, []);

  // const products = [
  //   {
  //     name: "Dota2",
  //     detail:
  //       "This is a wider card with supporting text below as a natural lead in to additional content. This content is a little bit longer.",
  //   },
  // ];

  const list = [];
  if(productList){
    productList.forEach((product) => {
      list.push(
        <div className="col">
          <div className="card shadow-sm">
            <img
              src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
              alt="avatar"
              className="img-fluid"
              style={{ width: "100%", height: "225px" }}
            />
            <div className="card-body">
              <h4>{product.productName}</h4>
              <p className="card-text">{product.productDetail}</p>
              <div className="d-flex justify-content-between align-items-center">
                <div className="btn-group">
                  <Link
                    className="btn btn-sm btn-outline-secondary btn-primary text-light"
                    to={"/product/" + product.product_id}
                  >
                    View
                  </Link>
                  <button
                    value={product.product_id}
                    className="btn btn-sm btn-outline-secondary btn-light text-dark"
                    onClick={(e) => buyProduct(e.target.value)}
                  >
                    Buy
                  </button>
                </div>
                <small className="text-muted">${product.productPrice}</small>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }
  return (
    <div>
      <Navbar />
      <div className="row row-cols-1 row-cols-sm-2 row-cols-md-4 g-4">
        {list}
      </div>
    </div>
  );
}

export default Store;
