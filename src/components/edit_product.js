import { useState, useEffect } from "react";
import Axios from "axios";
import "./register.css";
import Navbar from "./navbar";

import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

function EditProduct() {
  Axios.defaults.withCredentials = true;

  const [alreadyUpdateProfile, setAlreadyUpdateProfile] = useState(false);
  const [updateCheck, setUpdateCheck] = useState(true);

  const [product_id, setProduct_id] = useState();
  const [publisher_id,setPublisher_id] = useState();
  const [productName, setProductName] = useState("");
  const [productDetail, setProductDetail] = useState("");
  const [productPrice, setProductPrice] = useState(0);
  const [productDev, setProductDev] = useState("");

  const [productName2, setProductName2] = useState("");
  const [productDetail2, setProductDetail2] = useState("");
  const [productPrice2, setProductPrice2] = useState(0);
  const [productDev2, setProductDev2] = useState("");

  const [file, setFile] = useState();
  const [uploadCheck, setUploadCheck] = useState(false);

  const { product_url } = useParams();

  const uploadPhoto = (event) => {
    // create form data to send in multi-part form data (allow us to send photo via post method)
    const data = new FormData();
    data.append("name", product_id);
    data.append("file", file);
    console.log(data);
    Axios.post("https://steam-server-api.herokuapp.com/upload_product", data).then((res) => {
      if (res.data.isUpload) {
        console.log(res.data.message);
        setUploadCheck(true);
      } else {
        console.log(res.data.message);
        setUploadCheck(false);
      }
      console.log("Upload");
    });
    event.preventDefault();
  };

  useEffect(() => {
    Axios.get("https://steam-server-api.herokuapp.com/product/" + product_url).then((res) => {
      if (res.data.product_id) {
        setProduct_id(res.data.product_id);
        setPublisher_id(res.data.publisher_id);
        setProductName(res.data.productName);
        setProductDetail(res.data.productDetail);
        setProductPrice(res.data.productPrice);
        setProductDev(res.data.productDev);

        setProductName2(res.data.productName);
        setProductDetail2(res.data.productDetail);
        setProductPrice2(res.data.productPrice);
        setProductDev2(res.data.productDev);
        console.log("Product Name: ", res.data.productName);
      } else {
        console.log("No user info");
      }
    });
  }, []);

  const editProduct = (event) => {
    Axios.put("https://steam-server-api.herokuapp.com/edit_product", {
      product_id: product_id,
      publisher_id: publisher_id,
      productName: productName,
      productDetail: productDetail,
      productPrice: productPrice,
      productDev: productDev,
    }).then((res) => {
      if (res.data.isUpdate) {
        console.log(res.data.message);
        setAlreadyUpdateProfile(true);
        setUpdateCheck(true);
        console.log("Update Successfully");
      } else {
        console.log(res.data.message);
        console.log("Update Failed");
        setAlreadyUpdateProfile(false);
        setUpdateCheck(false);
      }
    });
    event.preventDefault();
  };

  if (alreadyUpdateProfile) {
    return (
      <div>
        <Navbar />
        <div className="login form-signin">
          <form>
            <Link
              className="w-100 btn btn-lg btn-primary"
              to={"/product/" + product_id}
            >
              Product
            </Link>
          </form>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <Navbar />

        <div className="login form-signin">
          <form>
            <div className="mb-3">
              <h1 class="h3 mb-3 fw-normal text-light" id="please_sign_in">
                Edit Product
              </h1>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="firstname"
                  onChange={(event) => {
                    setProductName(event.target.value);
                  }}
                />
                <label for="floatingInput">Product Name: {productName2}</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="lastname"
                  onChange={(event) => {
                    setProductDev(event.target.value);
                  }}
                />
                <label for="floatingInput">Developer: {productDev2}</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="email"
                  onChange={(event) => {
                    setProductPrice(event.target.value);
                  }}
                />
                <label for="floatingInput">Price: {productPrice2}</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="firstname"
                  onChange={(event) => {
                    setProductDetail(event.target.value);
                  }}
                />
                <label for="floatingInput">Detail</label>
              </div>
            </div>

            <input
              type="file"
              id="file"
              onChange={(event) => {
                const file = event.target.files[0];
                setFile(file);
              }}
            />
            <button
              className="w-50 btn btn-lg btn-primary"
              onClick={uploadPhoto}
            >
              Upload
            </button>
            {uploadCheck === true && (
              <p id="upload_success">Upload Successfully</p>
            )}
            <hr />

            {updateCheck === false && <p id="pass_incorrect">Update Failed</p>}
            <button
              className="w-100 btn btn-lg btn-primary"
              onClick={editProduct}
            >
              Update
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default EditProduct;
