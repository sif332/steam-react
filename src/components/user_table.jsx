import Axios from "axios";
import React from "react";
import { useState } from "react";

function GetUser() {
  const [userList, setUserList] = useState([]);

  const getUserList = () => {
    Axios.get("https://steam-server-api.herokuapp.com/user_table").then((respose) => {
      setUserList(respose.data);
    });
  };

  return (
    <div class="usertable">
      <button type="button" class="btn btn-primary" onClick={getUserList}>
        Show User Table
      </button>
      {userList.length > 0 && (
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Username</th>
              <th scope="col">Password</th>
            </tr>
          </thead>
          <tbody>
            {userList.map((val) => {
              return (
                <tr>
                  <th scope="row">{val.id}</th>
                  <td>{val.username}</td>
                  <td>{val.password}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      )}
    </div>
  );
}

export default GetUser;
