import { useState, useEffect } from "react";
import Axios from "axios";
import "./register.css";
import Navbar from "./navbar";

import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

function EditProfile() {
  Axios.defaults.withCredentials = true;

  const [alreadyUpdateProfile, setAlreadyUpdateProfile] = useState(false);
  const [updateCheck, setUpdateCheck] = useState(true);

  const [userid, setUserid] = useState();

  const [username, setUsername] = useState("None");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [payment, setPayment] = useState("");

  const [username2, setUsername2] = useState("None");
  const [firstname2, setFirstname2] = useState("");
  const [lastname2, setLastname2] = useState("");
  const [email2, setEmail2] = useState("");
  const [payment2, setPayment2] = useState("");

  const [file, setFile] = useState();
  const [uploadCheck, setUploadCheck] = useState(false);

  const { user_url } = useParams();

  const uploadPhoto = (event) => {
    // create form data to send in multi-part form data (allow us to send photo via post method)
    const data = new FormData();
    data.append("name", userid);
    data.append("file", file);
    console.log(data);
    Axios.post("https://steam-server-api.herokuapp.com/upload_profile", data).then((res) => {
      if(res.data.isUpload){
        console.log(res.data.message);
        setUploadCheck(true);
      }else{
        console.log(res.data.message);
        setUploadCheck(false);
      }
      console.log("Upload");
    });
    event.preventDefault();
  };

  useEffect(() => {
    Axios.get("https://steam-server-api.herokuapp.com/userinfo/"+user_url).then((res) => {
      if (res.data.username) {
        setUserid(res.data.user_id);
        setUsername(res.data.username);
        setFirstname(res.data.firstname);
        setLastname(res.data.lastname);
        setEmail(res.data.email);
        setPayment(res.data.payment);

        setUsername2(res.data.username);
        setFirstname2(res.data.firstname);
        setLastname2(res.data.lastname);
        setEmail2(res.data.email);
        setPayment2(res.data.payment);
        console.log("Username: ", res.data.username);
      } else {
        console.log("No user info");
      }
    });
  }, []);

  const editProfile = (event) => {
    Axios.put("https://steam-server-api.herokuapp.com/edit_profile", {
      firstname: firstname,
      lastname: lastname,
      email: email,
      payment: payment,
    }).then((res) => {
      if (res.data.isUpdate) {
        console.log(res.data.message);
        setAlreadyUpdateProfile(true);
        setUpdateCheck(true);
        console.log("Update Successfully");
      } else {
        console.log(res.data.message);
        console.log("Update Failed");
        setAlreadyUpdateProfile(false);
        setUpdateCheck(false);
      }
    });
    event.preventDefault();
  };

  if (alreadyUpdateProfile) {
    return (
      <div>
        <Navbar />
        <div className="login form-signin">
          <form>
            <Link className="w-100 btn btn-lg btn-primary" to={"/profile/"+username}>
              Profile
            </Link>
          </form>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <Navbar />

        <div className="login form-signin">
          <form>
            <div className="mb-3">
              <h1 class="h3 mb-3 fw-normal text-light" id="please_sign_in">
                Edit Profile
              </h1>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="firstname"
                  onChange={(event) => {
                    setFirstname(event.target.value);
                  }}
                />
                <label for="floatingInput">Firstname: {firstname2}</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="lastname"
                  onChange={(event) => {
                    setLastname(event.target.value);
                  }}
                />
                <label for="floatingInput">Lastname: {lastname2}</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="email"
                  onChange={(event) => {
                    setEmail(event.target.value);
                  }}
                />
                <label for="floatingInput">Email: {email2}</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="firstname"
                  onChange={(event) => {
                    setPayment(event.target.value);
                  }}
                />
                <label for="floatingInput">Payment Number</label>
              </div>
            </div>
            
              <input
                type="file"
                id="file"
                onChange={(event) => {
                  const file = event.target.files[0];
                  setFile(file);
                }}
              />
              <button
                className="w-50 btn btn-lg btn-primary"
                onClick={uploadPhoto}
              >
                Upload
              </button>
              {uploadCheck === true && <p id="upload_success">Upload Successfully</p>}
              <hr/>
            
            {updateCheck === false && <p id="pass_incorrect">Update Failed</p>}
            <button
              className="w-100 btn btn-lg btn-primary"
              onClick={editProfile}
            >
              Update
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default EditProfile;
