import React from "react";
import { Link, Route, useParams, useRouteMatch, Switch} from "react-router-dom";

function Category() {
  const { url, path } = useRouteMatch();

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <a className="navbar-brand">Steam</a>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link" to="/">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link active" to="/Store">
                  Store
                </Link>
              </li>
            </ul>
            <form className="d-flex">
              <Link className="btn btn-outline-success" to="/product">
                Login
              </Link>
            </form>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Category;
