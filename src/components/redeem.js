import { useState, useEffect } from "react";
import Axios from "axios";
import "./register.css";
import Navbar from "./navbar";

import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

function Redeem() {
  Axios.defaults.withCredentials = true;

  const [alreadyUpdateProfile, setAlreadyUpdateProfile] = useState(false);
  const [updateCheck, setUpdateCheck] = useState(true);

  const [redeemCode, setRedeemCode] = useState();

  const { user_url } = useParams();
  const [userid, setUserid] = useState();
  useEffect(() => {
    Axios.get("https://steam-server-api.herokuapp.com/userinfo/"+user_url).then((res) => {
      if (res.data.username) {
        setUserid(res.data.user_id);
        console.log("Username: ", res.data.username);
      } else {
        console.log("No user info");
      }
    });
  }, []);

  const sendRedeem = (event) => {
    Axios.put("https://steam-server-api.herokuapp.com/redeem", {
      redeemCode: redeemCode
    }).then((res) => {
      if (res.data.isUpdate) {
        console.log(res.data.message);
        setAlreadyUpdateProfile(true);
        setUpdateCheck(true);
        console.log("Redeem Successfully");
      } else {
        console.log(res.data.message);
        console.log("Redeem Failed");
        setAlreadyUpdateProfile(false);
        setUpdateCheck(false);
      }
    });
    event.preventDefault();
  };

  if (alreadyUpdateProfile) {
    return (
      <div>
        <Navbar />
        <div className="login form-signin">
          <form>
            <Link className="w-100 btn btn-lg btn-primary" to={"/profile/"+user_url}>
              Profile
            </Link>
          </form>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <Navbar />

        <div className="login form-signin">
          <form>
            <div className="mb-3">
              <h1 class="h3 mb-3 fw-normal text-light" id="please_sign_in">
              Redeem wallet code
              </h1>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="firstname"
                  onChange={(event) => {
                    setRedeemCode(event.target.value);
                  }}
                />
                <label for="floatingInput">Redeem Code</label>
              </div>
            </div>
            {updateCheck === false && <p id="pass_incorrect">Redeem Failed</p>}
            <button
              className="w-100 btn btn-lg btn-primary"
              onClick={sendRedeem}
            >
              Redeem
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Redeem;
