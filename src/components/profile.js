import Axios from "axios";
import Navbar from "./navbar";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

function Profile() {
  const { user_url } = useParams();
  const [userid, setUserid] = useState();
  const [username, setUsername] = useState("None");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [payment, setPayment] = useState("");
  const [balance, setBalance] = useState();
  const [profilePhoto, setProfilePhoto] = useState(0);
  const [isLogin, setIsLogin] = useState(false);
  const [isFound, setIsFound] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);

  const [productList, setProductList] = useState([]);
  const list = [];

  useEffect(() => {
    Axios.get("https://steam-server-api.herokuapp.com/userinfo/" + user_url).then((res) => {
      if (res.data.username) {
        setUserid(res.data.user_id);
        setUsername(res.data.username);
        setFirstname(res.data.firstname);
        setLastname(res.data.lastname);
        setEmail(res.data.email);
        setPayment(res.data.payment);
        setProfilePhoto(res.data.profile_photo);
        setBalance(res.data.balance);
        setIsLogin(res.data.isLogin);
        setIsFound(res.data.isFound);
        console.log("Username: ", res.data.username, res.data.user_id);
        Axios.get("https://steam-server-api.herokuapp.com/owned_game/" + res.data.user_id).then(
          (res) => {
            if (res.data) {
              setProductList(res.data.product_list);
              console.log("Product Found");
            } else {
              console.log("Product not found");
            }
          }
        );
      } else {
        console.log("No user info");
      }
    });
  }, []);

  if (productList) {
    productList.forEach((product) => {
      list.push(<p>{product.productName}</p>);
    });
  }

  if (!isFound) {
    return <p style={{ color: "white" }}>User not Found</p>;
  } else {
    return (
      <section>
        <Navbar />
        <div className="container py-5">
          <div className="row">
            <div className="col-lg-4">
              <div className="card mb-4">
                <div className="card-body text-center">
                  <img
                    src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                    alt="avatar"
                    className="rounded-circle img-fluid"
                    style={{ width: "150px", height: "150px" }}
                  />
                  <h5 className="my-3">{username}</h5>
                  <p className="text-muted mb-1">I'm a Gamer.</p>
                  <p className="text-muted mb-4">Game is Life</p>
                  {isLogin && (
                    <div className="d-flex justify-content-center mb-2">
                      <Link
                        className="btn btn-primary"
                        style={{ marginRight: "10px" }}
                        to={"/edit_profile/" + user_url}
                      >
                        Edit Profile
                      </Link>
                      <Link
                        className="btn btn-outline-dark" style={{ marginRight: "10px" }}
                        to={"/redeem/" + user_url}
                      >
                        Redeem Code
                      </Link>
                      <Link
                        className="btn btn-outline-dark"
                        to={"/register_product"}
                      >
                        Add Product
                      </Link>
                    </div>
                  )}
                  {isAdmin && (
                    <div className="d-flex justify-content-center mb-2">
                      <Link
                        className="btn btn-primary"
                        to={"/register_product"}
                      >
                        Add Product
                      </Link>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="col-lg-8">
              <div className="card mb-4">
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Firstname</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{firstname}</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Lastname</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{lastname}</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Email</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{email}</p>
                    </div>
                  </div>
                  {isLogin && (
                    <div>
                      <hr />
                      <div className="row">
                        <div className="col-sm-3">
                          <p className="mb-0">Payment</p>
                        </div>
                        <div className="col-sm-9">
                          <p className="text-muted mb-0">{payment}</p>
                        </div>
                      </div>
                      <hr />
                      <div className="row">
                        <div className="col-sm-3">
                          <p className="mb-0">Balance</p>
                        </div>
                        <div className="col-sm-9">
                          <p className="text-muted mb-0">{balance}</p>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <div className="card mb-4 mb-md-0">
                    <div className="card-body">
                      <h6 className="mb-4">Game Owned</h6>
                      {list}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Profile;
