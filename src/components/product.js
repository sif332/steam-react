import Axios from "axios";
import Navbar from "./navbar";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";

//for convert SQL timedate to readable text
const moment = require("moment");

function Product() {
  const { product_url } = useParams();

  const [userid, setUserid] = useState();
  const [productid, setproduct_id] = useState();
  const [publisher_id,setPublisher_id] = useState();
  const [productName, setProductName] = useState("None");
  const [productDetail, setproductDetail] = useState("");
  const [productPrice, setproductPrice] = useState(0);
  const [productDev, setproductDev] = useState("");
  const [productScore, setproductScore] = useState(0);
  const [productDate, setproductDate] = useState("");
  const [productImage, setproductImage] = useState(0);
  const [isLogin, setIsLogin] = useState(false);
  const [isFound, setIsFound] = useState(false);
  const [isAdmin, setIsAdmin] = useState(0);

  useEffect(() => {
    Axios.get("https://steam-server-api.herokuapp.com/product/" + product_url).then((res) => {
      if (res.data.product_id) {
        setUserid(res.data.user_id);
        setproduct_id(res.data.product_id);
        setPublisher_id(res.data.publisher_id);
        setProductName(res.data.productName);
        setproductDetail(res.data.productDetail);
        setproductPrice(res.data.productPrice);
        setproductDev(res.data.productDev);
        setproductScore(res.data.productScore);
        setproductImage(res.data.productImage);
        setIsLogin(res.data.isLogin);
        setIsFound(res.data.isFound);
        setIsAdmin(res.data.isAdmin);
        let date = moment(res.data.productDate).format('DD-MM-YYYY');
        setproductDate(date);
        console.log("Product: ", res.data.productName, res.data.product_id);
        console.log("IsLogin: ", res.data.isLogin);
      } else {
        console.log("No Product info");
      }
    });
  }, []);

  function buyProduct(myproduct_id) {
    Axios.get("https://steam-server-api.herokuapp.com/buy/" + myproduct_id).then((res) => {
      if (res.data) {
      } else {
      }
    });
  }

  if (!isFound) {
    return <p style={{ color: "white" }}>Product not Found</p>;
  } else {
    return (
      <section>
        <Navbar />
        <div className="container py-5">
          <div className="row">
            <div className="col-lg-4">
              <div className="card mb-4">
                <div className="card-body text-center">
                  <img
                    src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                    alt="avatar"
                    className="rounded-circle img-fluid"
                    style={{ width: "150px", height: "150px" }}
                  />
                  <h5 className="my-3">{productName}</h5>
                  <p className="text-muted mb-1">{productDev}</p>
                  <p className="text-muted mb-4">${productPrice}</p>
                  <div className="d-flex justify-content-center mb-2">
                  <button
                    value={productid}
                    className="btn btn-primary"
                    onClick={(e) => buyProduct(e.target.value)}
                  >
                    Buy
                  </button>
                  {(isAdmin === 1 || userid === publisher_id) && isLogin && (
                    
                      <Link
                        className="btn btn-outline-dark"
                        style={{marginLeft: "10px"}}
                        to={"/edit_product/" + productid}
                      >
                        Edit Product
                      </Link>
                    
                  )}
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-8">
              <div className="card mb-4">
                <div className="card-body">
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Release Date</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{productDate}</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Product Score</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{productScore}</p>
                    </div>
                  </div>
                  <hr />
                  <div className="row">
                    <div className="col-sm-3">
                      <p className="mb-0">Product Detail</p>
                    </div>
                    <div className="col-sm-9">
                      <p className="text-muted mb-0">{productDetail}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Product;
