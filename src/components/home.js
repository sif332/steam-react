import Axios from "axios";
import Navbar from "./navbar"

Axios.defaults.withCredentials = true;

function Home() {
  
  return (
    <div>
      <Navbar />
      <img src="https://cdn.cloudflare.steamstatic.com/store/home/store_home_share.jpg" alt="Steam" style={{width: "100%"}} />
    </div>
  );
}

export default Home;
