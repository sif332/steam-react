import { useState } from "react";
import Axios from "axios";
import "./register.css";
import Navbar from "./navbar";

import { Link } from "react-router-dom";

function Register_Product() {
  Axios.defaults.withCredentials = true;
  const [alreadyRegister, setAlreadyRegister] = useState(false);
  const [registerCheck, setRegisterCheck] = useState(true);

  const [productName, setProductName] = useState("");
  const [productDetail, setProductDetail] = useState("");
  const [productPrice, setProductPrice] = useState(0);
  const [productDev, setProductDev] = useState("");

  const addProduct = (event) => {
    Axios.post("https://steam-server-api.herokuapp.com/register_product", {
      productName: productName,
      productDetail: productDetail,
      productPrice: parseInt(productPrice),
      productDev: productDev
    }).then((res) => {
      if(res.data.isRegister){
        console.log(res.data.message);
        setRegisterCheck(true);
        setAlreadyRegister(true);
        console.log("Register Successfully");
      }else{
        console.log(res.data.message);
        console.log("Duplicate Username");
        setRegisterCheck(false);
        setAlreadyRegister(false);
      }
    });
    event.preventDefault();
  };

  if (alreadyRegister) {
    return (
      <div>
        <Navbar />
        <div className="login form-signin">
          <form>
            <Link className="w-100 btn btn-lg btn-primary" to="/store">
              Store
            </Link>
          </form>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <Navbar />

        <div className="login form-signin">
          <form>
            <div className="mb-3">
              <h1 class="h3 mb-3 fw-normal text-light" id="please_sign_in">
                Product Registration
              </h1>
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="username"
                  onChange={(event) => {
                    setProductName(event.target.value);
                  }}
                />
                <label for="floatingInput">Product Name</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="username"
                  onChange={(event) => {
                    setProductDetail(event.target.value);
                  }}
                />
                <label for="floatingInput">Product Detail</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="username"
                  onChange={(event) => {
                    setProductDev(event.target.value);
                  }}
                />
                <label for="floatingInput">Product Developer</label>
              </div>
            </div>
            <div className="mb-3">
              <div class="form-floating">
                <input
                  className="form-control"
                  id="floatingInput"
                  placeholder="username"
                  onChange={(event) => {
                    setProductPrice(event.target.value);
                  }}
                />
                <label for="floatingInput">Product Price</label>
              </div>
            </div>
            {registerCheck === false && <p id="pass_incorrect" >Registeration Failed</p>}
            <button
              className="w-100 btn btn-lg btn-primary"
              onClick={addProduct}
            >
              Register
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Register_Product;
