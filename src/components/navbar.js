import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Axios from "axios";

Axios.defaults.withCredentials = true;

var globalLogState = false;

function Navbar() {
  const [logState, setLogIn] = useState(false);
  const [user_url, setUserUrl] = useState("None");

  const logOut = () => {
    Axios.get("https://steam-server-api.herokuapp.com/logout").then((res) => {
      if (res.data.isLogin) {
        setLogIn(true);
        globalLogState = true;
        console.log("Axios", logState);
      } else {
        setLogIn(false);
        globalLogState = false;
        console.log("Axios", logState);
      }
    });
  };

  useEffect(() => {
    Axios.get("https://steam-server-api.herokuapp.com/login").then((res) => {
      if (res.data.isLogin) {
        setLogIn(true);
        setUserUrl(res.data.user_url);
        globalLogState = true;
        console.log("Axios", logState);
      } else {
        setLogIn(false);
        globalLogState = false;
        console.log("Axios", logState);
      }
    });
  }, []);

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <a className="navbar-brand">Steam</a>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link active" to="/">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/store">
                  Store
                </Link>
              </li>
            </ul>
            <form className="d-flex">
              {!globalLogState ? (
                <div>
                <Link className="btn btn-success" style={{marginRight: 10}} to={"/cart"}>
                    Cart
                  </Link>
                <Link className="btn btn-outline-success" to="/login">
                  Login
                </Link>
                </div>
              ) : (
                <div>
                <Link className="btn btn-success" style={{marginRight: 10}} to={"/cart"}>
                    Cart
                  </Link>
                  <Link className="btn btn-success" style={{marginRight: 10}} to={"/profile/"+user_url}>
                    Profile
                  </Link>
                  <button className="btn btn-outline-success" onClick={logOut}>
                    Logout
                  </button>
                </div>
              )}
            </form>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
export { globalLogState };
