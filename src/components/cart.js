import Axios from "axios";
import Navbar from "./navbar";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function Cart() {
  const [productList, setProductList] = useState([]);
  const [logState, setLogIn] = useState(false);
  const [user_url, setUserUrl] = useState("None");
  const [isPurchase, setIsPurchase] = useState(false);
  const [isFail, setIsFail] = useState(false);
  const [message, setMessage] = useState("");
  const list = [];

  const purchase = () => {
    Axios.post("https://steam-server-api.herokuapp.com/purchase").then((res) => {
      if (res.data) {
        setIsPurchase(res.data.purchase_status);
        setIsFail(res.data.purchase_issue);
        console.log(res.data.purchase_status);
        if (res.data.purchase_status) {
          Axios.get("https://steam-server-api.herokuapp.com/clear_buy_list").then((res2) => {});
        }
      } else {
      }
    });
  };

  function delete_buy_list(myproduct_id) {
    Axios.put("https://steam-server-api.herokuapp.com/delete_buy_list/" + myproduct_id).then(
      (res) => {
        if (res.data) {
          window.location.reload();
        } else {
        }
      }
    );
    //refresh page
    window.location.reload();
  }

  useEffect(() => {
    Axios.get("https://steam-server-api.herokuapp.com/cart").then((res) => {
      if (res.data) {
        setProductList(res.data.product_list);
        console.log("Cart Found");
      } else {
        console.log("Cart not found");
      }
    });
    Axios.get("https://steam-server-api.herokuapp.com/login").then((res) => {
      if (res.data.isLogin) {
        setLogIn(true);
        setUserUrl(res.data.user_url);
        console.log("Axios", logState);
      } else {
        setLogIn(false);
        console.log("Axios", logState);
      }
    });
  }, []);
  if (productList) {
    productList.forEach((product) => {
      list.push(
        <div className="card rounded-3 mb-4">
          <div className="card-body p-4">
            <div className="row justify-content-between align-items-center d-flex">
              <div className="col-md-2 col-lg-2 col-xl-2">
                <img
                  src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava3.webp"
                  alt="avatar"
                  className="img-fluid rounded-3"
                />
              </div>
              <div className="col-md-3 col-lg-3 col-xl-3">
                <p className="lead fw-normal mb-2">{product.productName}</p>
                <p>{product.productDev}</p>
              </div>
              <div className="col-md-3 col-lg-2 col-xl-2 d-flex"></div>
              <div className="col-md-3 col-lg-2 col-xl-2 offset-lg-1 d-inline">
                <h5 className="mb-0">${product.productPrice}</h5>
              </div>
              <div class="col-md-1 col-lg-2 col-xl-1">
                <button
                  type="button"
                  className="btn btn-danger"
                  value={product.product_id}
                  onClick={(e) => delete_buy_list(e.target.value)}
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }
  if (isPurchase) {
    return (
      <div>
        <Navbar />
        <div className="login form-signin">
          <form>
            <Link className="w-100 btn btn-lg btn-primary" to="/">
              Successful Back to Home
            </Link>
          </form>
        </div>
      </div>
    );
  } else {
    return (
      <section className="h-100">
        <Navbar />
        <div className="container h-100 py-5">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-10">
              <div className="d-flex justify-content-between align-items-center mb-4">
                <h3 className="fw-normal mb-0 text-light">Shopping Cart</h3>
                <div></div>
              </div>
              {list}
              <div className="card align-items-center">
                <div className="card-body">
                  {isFail && (
                    <p style={{ color: "red" }}>
                      Purchase Failed: Maybe duplicate product or Not enough balance
                    </p>
                  )}
                  {logState ? (
                    <button
                      type="button"
                      className="btn btn-warning btn-lg"
                      onClick={purchase}
                    >
                      Purchase
                    </button>
                  ) : (
                    <p>Please Login before Purchase</p>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Cart;
